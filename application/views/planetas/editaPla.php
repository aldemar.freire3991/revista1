<h1 Align="center">Editar REVISTAS</h1>

<form class="" id="frm_editar_planeta" action="<?php echo site_url(); ?>/Planetas/editarPla" method="post">
    <div class="container">
        <div class="row">
            <input type="text" class="form-control" name="ID_Revista" id="ID_Revista" hidden value="<?php echo $editaPlan->ID_Revista; ?>">
            <div class="col-md-4">
                <label for="">Nombre: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del Planeta" class="form-control" name="Nombre" required value="<?php echo $editaPlan->Nombre; ?>">
            </div>
            <div class="col-md-4">
                <label for=""> ISSN: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar el orden del Planeta" class="form-control" required name="ISSN" value="<?php echo $editaPlan->ISSN; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> SITIO WEB: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Sitio_Web" value="<?php echo $editaPlan->Sitio_Web; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> FECHA DE FUNDACION: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar la distancia del Planeta" class="form-control" required name="Fecha_Fundacion" value="<?php echo $editaPlan->Fecha_Fundacion; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> TEMA: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el estado del Planeta" class="form-control" required name="Tema" value="<?php echo $editaPlan->Tema; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="ID_Comite">Comite: </label>
                <select name="ID_Comite" class="form-control" id="ID_Comite" required>
                    <?php foreach ($revista as $revista) : ?>
                        <option value="<?php echo $revista->ID_Comite; ?>"><?php echo $revista->Nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/planetas/listPla" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_editar_planeta").validate({
        rules: {
            nom_pla_recup_ba: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            orden_pla_recup_ba: {
                required: true,
                minlength: 1,
                maxlength: 3,
                digits: true,
            },
            foto_pla_recup_: {
                required: true,
                minlength: 5,
                maxlength: 250,
                letras: true,
            },
            dista_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 20,
                digits: true,
            },
            esta_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 100,
                letras: true,
            }
        },
        messages: {
            nom_pla_recup_ba: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            orden_pla_recup_ba: {
                required: "Por favor ingrese el Orden del planeta",
                minlength: "El Orden debe tener al menos 1 digito",
                maxlength: "Orden Incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            foto_pla_recup_: {
                required: "Por favor ingresar la ciudad",
                minlength: "El apellido debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",
            },
            dista_pla_recup_ba: {
                required: "Por favor ingresa un número de distancia",
                minlength: "Distancia incorrecta ingrese al menos 5 digitos",
                maxlength: "Distancia incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            esta_pla_recup_ba: {
                required: "Por favor ingresa un estado",
                minlength: "El estado debe tener al menos 5 caracteres",
                maxlength: "Estado muy extenso muy Extensa",

            }
        }
    });
</script>