<h1 Align="center">Listar Revistas</h1>

<?php if($revista): ?>
    <table class="table table-striped text-center" id="tbl_planetas" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>ISSN</th>
                <th>Sitio_Web</th>
                <th>Fecha de fundacion</th>
                <th>tema</th>
                <th>comite</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($revista as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->ID_Revista; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Nombre; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ISSN; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Sitio_Web; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Fecha_Fundacion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Tema; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ID_Comite; ?>
                    </td>

                    <td >
                        <a href="<?php echo site_url(); ?>/planetas/editaPla/<?php echo $filaTemporal->ID_Revista; ?>" title="Editar Galaxia">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Planetas/eliminaPla/<?php echo $filaTemporal->ID_Revista; ?>" title="Eliminar Galaxia">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE PLANETAS ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_planetas").DataTable();
</script>
