<h1 Align="center">Editar articulos</h1>

<form class="" id="frm_editar_articulo" action="<?php echo site_url(); ?>/Articulos/editarAr" method="post">
    <div class="container">
        <div class="row">
            <input type="text" class="form-control" name="ID_Articulo" id="ID_Articulo" hidden value="<?php echo $editaArt->ID_Articulo; ?>">
            <div class="col-md-4">
                <label for="">TITULO: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del patrocinador" class="form-control" name="Titulo" required value="<?php echo $editaArt->Titulo; ?>">
            </div>
            <div class="col-md-4">
                <label for=""> RESUMEN: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el orden del Planeta" class="form-control" required name="Resumen" value="<?php echo $editaArt->Resumen; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="">  PALABRAS CLAVE: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Palabras_Clave" value="<?php echo $editaArt->Palabras_Clave; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="ID_Revista">Revista: </label>
                <select name="ID_Revista" class="form-control" id="ID_Revista" required>
                    <?php foreach ($articulo as $articulo) : ?>
                        <option value="<?php echo $articulo->ID_Revista; ?>"><?php echo $articulo->Nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/articulos/listAr" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_editar_articulo").validate({
        rules: {
            Titulo: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            Resumen: {
                required: true,
                minlength: 1,
                maxlength: 30,
                letras: true,
            },
            Palabras_Clave: {
                required: true,
                minlength: 5,
                maxlength: 20,
                letras: true,
            },
            ID_Revista: {
                required: true,
            }
        },
        messages: {
            Titulo: {
                required: "Por favor ingresar el titulo",
                minlength: "El titulo debe tener al menos 3 caracteres",
                maxlength: "titulo incompleto",

            },
            Resumen: {
                required: "Por favor ingrese el resumen",
                minlength: "El resumen debe tener al menos 1 digito",
                maxlength: "resumen Incorrecta",
                
            },
            Palabras_Clave: {
                required: "Por favor las palabras clave",
                minlength: "las palabras clave deben ser al menos 5 digitos",
                maxlength: "las palabras clave deben ser  menor a 20 digitos ",
                
            },
            ID_Revista: {
                required: "Debes escoger una opcion",

            }
        }
    });
</script>
