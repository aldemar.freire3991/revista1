<h1 Align="center">Agregar Nuevo Articulo</h1>

<form class="" id="frm_nuevo_articulo" action="<?php echo site_url(); ?>/Articulos/guardaAr" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <label for="">titulo: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el titulo" class="form-control" name="Titulo" required value="">
            </div>
            <div class="col-md-4">
                <label for=""> resumen: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el resumen" class="form-control" required name="Resumen" value="">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> palabras clave: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar las palabras clave" class="form-control" required name="Palabras_Clave" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for="ID_Revista">Revista: </label>
                <select name="ID_Revista" class="form-control" id="ID_Revista" required>
                    <?php foreach ($articulo as $articulo) : ?>
                        <option value="<?php echo $articulo->ID_Revista; ?>"><?php echo $articulo->Nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            
            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/articulos/listAr" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_nuevo_articulo").validate({
        rules: {
            Titulo: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            Resumen: {
                required: true,
                minlength: 1,
                maxlength: 30,
                letras: true,
            },
            Palabras_Clave: {
                required: true,
                minlength: 5,
                maxlength: 20,
                letras: true,
            },
            ID_Revista: {
                required: true,
                minlength: 1,
                maxlength: 20,
                digits: true,
            }
        },
        messages: {
            Titulo: {
                required: "Por favor ingresar el titulo",
                minlength: "El titulo debe tener al menos 3 caracteres",
                maxlength: "titulo incompleto",

            },
            Resumen: {
                required: "Por favor ingrese el resumen",
                minlength: "El resumen debe tener al menos 1 digito",
                maxlength: "resumen Incorrecta",
                
            },
            Palabras_Clave: {
                required: "Por favor las palabras clave",
                minlength: "las palabras clave deben ser al menos 5 letras",
                maxlength: "las palabras clave deben ser  menor a 20 letras ",
                
            },
            ID_Revista: {
                required: "Debes escoger una opcion",
                minlength: "la revista   deben ser al menos 2 letras",
                maxlength: "las revista deben ser  menor a 20 letras ",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",

            }
        }
    });
</script>

