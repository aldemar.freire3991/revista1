<h1 Align="center">Listar articulos</h1>

<?php if($articulo): ?>
    <table class="table table-striped text-center" id="tbl_planetas" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>TITULO</th>
                <th>RESUMEN</th>
                <th>PALABRAS CLAVE</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($articulo as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->ID_Articulo; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Titulo; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Resumen; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Palabras_Clave; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ID_Revista; ?>
                    </td>

                    <td >
                        <a href="<?php echo site_url(); ?>/articulos/editaAr/<?php echo $filaTemporal->ID_Articulo; ?>" title="Editar articulo">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Articulos/eliminaAr/<?php echo $filaTemporal->ID_Articulo; ?>" title="Eliminar articulo">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE publicaciones ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_planetas").DataTable();
</script>
