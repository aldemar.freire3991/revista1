<h1 Align="center">Editar patrocinadores</h1>

<form class="" id="frm_editar_patrocinador" action="<?php echo site_url(); ?>/Patrocinadores/editarPa" method="post">
    <div class="container">
        <div class="row">
            <input type="text" class="form-control" name="ID_Patrocinador" id="ID_Patrocinador" hidden value="<?php echo $editaPat->ID_Patrocinador; ?>">
            <div class="col-md-4">
                <label for="">Nombre: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del patrocinador" class="form-control" name="Nombre" required value="<?php echo $editaPat->Nombre; ?>">
            </div>
            <div class="col-md-4">
                <label for=""> Sector: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar el orden del Planeta" class="form-control" required name="Sector" value="<?php echo $editaPat->Sector; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="">  TIPO: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Tipo" value="<?php echo $editaPat->Tipo; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for=""> CONTACTO: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar la distancia del Planeta" class="form-control" required name="Contacto" value="<?php echo $editaPat->Contacto; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="ID_Revista">Revista: </label>
                <select name="ID_Revista" class="form-control" id="ID_Revista" required>
                    <?php foreach ($patrocinador as $patrocinador) : ?>
                        <option value="<?php echo $patrocinador->ID_Revista; ?>"><?php echo $patrocinador->Nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/patrocinadores/listPa" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_editar_planeta").validate({
        rules: {
            nom_pla_recup_ba: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            orden_pla_recup_ba: {
                required: true,
                minlength: 1,
                maxlength: 3,
                digits: true,
            },
            foto_pla_recup_: {
                required: true,
                minlength: 5,
                maxlength: 250,
                letras: true,
            },
            dista_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 20,
                digits: true,
            },
            esta_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 100,
                letras: true,
            }
        },
        messages: {
            nom_pla_recup_ba: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            orden_pla_recup_ba: {
                required: "Por favor ingrese el Orden del planeta",
                minlength: "El Orden debe tener al menos 1 digito",
                maxlength: "Orden Incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            foto_pla_recup_: {
                required: "Por favor ingresar la ciudad",
                minlength: "El apellido debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",
            },
            dista_pla_recup_ba: {
                required: "Por favor ingresa un número de distancia",
                minlength: "Distancia incorrecta ingrese al menos 5 digitos",
                maxlength: "Distancia incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            esta_pla_recup_ba: {
                required: "Por favor ingresa un estado",
                minlength: "El estado debe tener al menos 5 caracteres",
                maxlength: "Estado muy extenso muy Extensa",

            }
        }
    });
</script>