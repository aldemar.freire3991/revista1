<h1 Align="center">Listar Patrocinadores</h1>

<?php if($patrocinador): ?>
    <table class="table table-striped text-center" id="tbl_planetas" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>SECTOR</th>
                <th>TIPO</th>
                <th>CONTACTO</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($patrocinador as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->ID_Patrocinador; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Nombre; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Sector; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Tipo; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Contacto; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ID_Revista; ?>
                    </td>

                    <td >
                        <a href="<?php echo site_url(); ?>/patrocinadores/editaPa/<?php echo $filaTemporal->ID_Patrocinador; ?>" title="Editar patrocinador">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Patrocinadores/eliminaPa/<?php echo $filaTemporal->ID_Patrocinador; ?>" title="Eliminar patrocinador">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE patrocinadores ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_planetas").DataTable();
</script>
