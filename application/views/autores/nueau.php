<h1 Align="center">Agregar nuevo autor</h1>

<form class="" id="frm_nueva_autor" action="<?php echo site_url(); ?>/Autores/guardaAu" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <label for="">Nombre: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del autor" class="form-control" name="Nombre" required value="">
            </div>
            <div class="col-md-6">
                <label for="">Afiliacion: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar afiliacion del autor" class="form-control" name="Afiliacion" required value="">
            </div>
            <div class="col-md-6">
                <label for=""> Correo EElectronico: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el correo del autor" class="form-control" required name="Correo_Electronico" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> FECHA NACIMEINTO: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar lafecha de nacimiento" class="form-control" required name="Fecha_Nacimiento" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> PAIS: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el pais de origen" class="form-control" required name="Pais" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> GENERO: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el genero" class="form-control" required name="Genero" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> AREA ESPECIALIZACION: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el area de especializacion" class="form-control" required name="Area_Especializacion" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for="">  ID REVISTA : <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar la revista principa ID" class="form-control" required name="ID_Revista_Principal" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> NUMERO PUBLICACIONES: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar el numero de publicaciones" class="form-control" required name="Numero_Publicaciones" value="">
                <br>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/autores/lisau" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_nueva_autor").validate({
        rules: {
            Nombre: {
                required: true,
                minlength: 3,
                maxlength: 50,
                letras: true,
            },
            Afiliacion: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Correo_Electronico: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Fecha_Nacimiento: {
                required: true,
                minlength: 3,
                maxlength: 200,
            },
            
            Pais: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Genero: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Area_Especializacion: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            ID_Revista_Principal: {
                required: true,
                minlength: 3,
                maxlength: 200,
                digits: true,
            },
            Numero_Publicaciones: {
                required: true,
                minlength: 1,
                maxlength: 200,
                digits: true,
            }
        },
        messages: {
            Nombre: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            Afiliacion: {
                required: "Por favor ingrese una afiliacion",
                minlength: "La afiliacion debe tener al menos 3 caracteres",
                maxlength: "afiliacion incorrecta",
            },
            Correo_Electronico: {
                required: "Por favor ingresar un correo",
                minlength: "El correo debe tener al menos 3 caracteres",
                maxlength: "correo incorrecto",

            },
            Fecha_Nacimiento: {
                required: "Por favor ingresar una fecha",
                minlength: "la fecha debe tener al menos 3 caracteres",
                maxlength: "fecha incorrecto",

            },
            
            Pais: {
                required: "Por favor ingresar un pais",
                minlength: "El pais debe tener al menos 3 caracteres",
                maxlength: "pais incorrecto",

            },
            Genero: {
                required: "Por favor ingrese una genero",
                minlength: "el genero debe tener al menos 3 caracteres",
                maxlength: "genero incorrecta",
            },
            Area_Especializacion: {
                required: "Por favor ingresar un area",
                minlength: "El area debe tener al menos 3 caracteres",
                maxlength: "area incorrecto",

            },
            ID_Revista_Principal: {
                required: "Por favor ingrese una  id revista",
                minlength: "La id revista debe tener al menos 3 caracteres",
                maxlength: "id revista incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            Numero_Publicaciones: {
                required: "Por favor ingresar un numero",
                minlength: "El numero debe tener al menos 1 caracteres",
                maxlength: "numero incorrecto",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",

            }
        }
    });
</script>