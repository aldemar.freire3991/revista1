<h1 Align="center">Editar Autores</h1>

<form class="" id="frm_editar_autor" action="<?php echo site_url(); ?>/Autores/editarAut" method="post">
    <div class="container">
        <div class="row">
            <input type="text" class="form-control" name="ID_Autor" id="ID_Autor" hidden value="<?php echo $editaAut->ID_Autor; ?>">

            <div class="col-md-6">
                <label for="">Nombre: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre de la galaxia" class="form-control" name="Nombre" value="<?php echo $editaAut->Nombre; ?>">
            </div>
            <div class="col-md-6">
                <label for="">Afiliacion: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre de la galaxia" class="form-control" name="Afiliacion" value="<?php echo $editaAut->Afiliacion; ?>">
            </div>
            <div class="col-md-6">
                <label for=""> Correo Electronico: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Correo_Electronico" value="<?php echo $editaAut->Correo_Electronico; ?>">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> Fecha Nacimiento: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Fecha_Nacimiento" value="<?php echo $editaAut->Fecha_Nacimiento; ?>">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> Pais: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Pais" value="<?php echo $editaAut->Pais; ?>">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> Genero : <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Genero" value="<?php echo $editaAut->Genero; ?>">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> Area especializacion : <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Area_Especializacion" value="<?php echo $editaAut->Area_Especializacion; ?>">
                <br>
            </div>
            <div class="col-md-6">
                <label for=""> Revista princial : <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="ID_Revista_Principal" value="<?php echo $editaAut->ID_Revista_Principal; ?>">
                <br>
            </div>
            <div class="col-md-6">
                <label for="">  Numero Publicaciones: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Numero_Publicaciones" value="<?php echo $editaAut->Numero_Publicaciones; ?>">
                <br>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR CAMBIOS
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/autores/lisau" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
        $("#frm_editar_autor").validate({
            rules: {
            Nombre: {
                required: true,
                minlength: 3,
                maxlength: 50,
                letras: true,
            },
            Afiliacion: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Correo_Electronico: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Fecha_Nacimiento: {
                required: true,
                minlength: 3,
                maxlength: 200,
            },
            
            Pais: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Genero: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            Area_Especializacion: {
                required: true,
                minlength: 3,
                maxlength: 200,
                letras: true,
            },
            ID_Revista_Principal: {
                required: true,
                minlength: 3,
                maxlength: 200,
                digits: true,
            },
            Numero_Publicaciones: {
                required: true,
                minlength: 1,
                maxlength: 200,
                digits: true,
            }
        },
        messages: {
            Nombre: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            Afiliacion: {
                required: "Por favor ingrese una afiliacion",
                minlength: "La afiliacion debe tener al menos 3 caracteres",
                maxlength: "afiliacion incorrecta",
            },
            Correo_Electronico: {
                required: "Por favor ingresar un correo",
                minlength: "El correo debe tener al menos 3 caracteres",
                maxlength: "correo incorrecto",

            },
            Fecha_Nacimiento: {
                required: "Por favor ingresar una fecha",
                minlength: "la fecha debe tener al menos 3 caracteres",
                maxlength: "fecha incorrecto",

            },
            
            Pais: {
                required: "Por favor ingresar un pais",
                minlength: "El pais debe tener al menos 3 caracteres",
                maxlength: "pais incorrecto",

            },
            Genero: {
                required: "Por favor ingrese una genero",
                minlength: "el genero debe tener al menos 3 caracteres",
                maxlength: "genero incorrecta",
            },
            Area_Especializacion: {
                required: "Por favor ingresar un area",
                minlength: "El area debe tener al menos 3 caracteres",
                maxlength: "area incorrecto",

            },
            ID_Revista_Principal: {
                required: "Por favor ingrese una  id revista",
                minlength: "La id revista debe tener al menos 3 caracteres",
                maxlength: "id revista incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            Numero_Publicaciones: {
                required: "Por favor ingresar un numero",
                minlength: "El numero debe tener al menos 1 caracteres",
                maxlength: "numero incorrecto",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",

            }
        }
    });
</script>