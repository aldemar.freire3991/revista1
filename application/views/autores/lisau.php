<h1 Align="center">Listar Autores</h1>

<?php if($autor): ?>
    <table class="table table-striped text-center" id="tbl_galaxias" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>Afiliacion</th>
                <th>CORREO</th>
                <th>FECHA NACIMIENTO</th>
                <th>PAIS</th>
                <th>GENERO</th>
                <th>AREA ESPECIALIZACION</th>
                <th>REVISTA PRINCIPAL</th>
                <th>NUMERO PUBLICACIONES</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($autor as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->ID_Autor; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Nombre; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Afiliacion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Correo_Electronico; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Fecha_Nacimiento; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Pais; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Genero; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Area_Especializacion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ID_Revista_Principal; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Numero_Publicaciones; ?>
                    </td>
                    <td >
                        <a href="<?php echo site_url(); ?>/autores/editaAu/<?php echo $filaTemporal->ID_Autor; ?>" title="Editar autor">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Autores/eliminaAu/<?php echo $filaTemporal->ID_Autor; ?>" title="Eliminar autor">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE autores ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_galaxias").DataTable();
</script>

