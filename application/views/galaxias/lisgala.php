<h1 Align="center">Listar Galaxias</h1>

<?php if($comite): ?>
    <table class="table table-striped text-center" id="tbl_galaxias" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DIRECCION</th>
                <th>DESCRIPCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($comite as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->ID_Comite; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Nombre; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Direccion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Descripcion; ?>
                    </td>
                    <td >
                        <a href="<?php echo site_url(); ?>/galaxias/editaGala/<?php echo $filaTemporal->ID_Comite; ?>" title="Editar Galaxia">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Galaxias/eliminaGala/<?php echo $filaTemporal->ID_Comite; ?>" title="Eliminar Galaxia">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE GALAXIAS ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_galaxias").DataTable();
</script>

