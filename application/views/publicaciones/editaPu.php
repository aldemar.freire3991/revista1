<h1 Align="center">Editar publicaciones</h1>

<form class="" id="frm_editar_publicacion" action="<?php echo site_url(); ?>/Publicaciones/editarPu" method="post">
    <div class="container">
        <div class="row">
            <input type="text" class="form-control" name="ID_Publicacion" id="ID_Publicacion" hidden value="<?php echo $editaPub->ID_Publicacion; ?>">
            <div class="col-md-4">
                <label for="">FECHA: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del patrocinador" class="form-control" name="Fecha_Publicacion" required value="<?php echo $editaPub->Fecha_Publicacion; ?>">
            </div>
            <div class="col-md-4">
                <label for=""> Volumen: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar el orden del Planeta" class="form-control" required name="Volumen" value="<?php echo $editaPub->Volumen; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="">  Numero: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar una Descripcion de la galaxia" class="form-control" name="Numero" value="<?php echo $editaPub->Numero; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="ID_Revista">Revista: </label>
                <select name="ID_Revista" class="form-control" id="ID_Revista" required>
                    <?php foreach ($publicacion as $publicacion) : ?>
                        <option value="<?php echo $publicacion->ID_Revista; ?>"><?php echo $publicacion->Nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/publicaciones/listPu" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_editar_planeta").validate({
        rules: {
            nom_pla_recup_ba: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            orden_pla_recup_ba: {
                required: true,
                minlength: 1,
                maxlength: 3,
                digits: true,
            },
            foto_pla_recup_: {
                required: true,
                minlength: 5,
                maxlength: 250,
                letras: true,
            },
            dista_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 20,
                digits: true,
            },
            esta_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 100,
                letras: true,
            }
        },
        messages: {
            nom_pla_recup_ba: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            orden_pla_recup_ba: {
                required: "Por favor ingrese el Orden del planeta",
                minlength: "El Orden debe tener al menos 1 digito",
                maxlength: "Orden Incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            foto_pla_recup_: {
                required: "Por favor ingresar la ciudad",
                minlength: "El apellido debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",
            },
            dista_pla_recup_ba: {
                required: "Por favor ingresa un número de distancia",
                minlength: "Distancia incorrecta ingrese al menos 5 digitos",
                maxlength: "Distancia incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            esta_pla_recup_ba: {
                required: "Por favor ingresa un estado",
                minlength: "El estado debe tener al menos 5 caracteres",
                maxlength: "Estado muy extenso muy Extensa",

            }
        }
    });
</script>