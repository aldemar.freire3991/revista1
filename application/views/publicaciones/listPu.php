<h1 Align="center">Listar Publicaciones</h1>

<?php if($publicacion): ?>
    <table class="table table-striped text-center" id="tbl_planetas" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>FECHA</th>
                <th>VOLUMEN</th>
                <th>NUMERO</th>
                <th>REVISTA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($publicacion as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->ID_Publicacion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Fecha_Publicacion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Volumen; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Numero; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ID_Revista; ?>
                    </td>

                    <td >
                        <a href="<?php echo site_url(); ?>/publicaciones/editaPu/<?php echo $filaTemporal->ID_Publicacion; ?>" title="Editar publicacion">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Publicaciones/eliminaPu/<?php echo $filaTemporal->ID_Publicacion; ?>" title="Eliminar publicacion">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE publicaciones ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_planetas").DataTable();
</script>
