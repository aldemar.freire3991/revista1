<h1 Align="center">Editar contribuciones</h1>

<form class="" id="frm_editar_contribuciones" action="<?php echo site_url(); ?>/Contribuciones/editarCo" method="post">
    <div class="container">
        <div class="row">
            <input type="text" class="form-control" name="ID_Contribucion" id="ID_Contribucion" hidden value="<?php echo $editaCon->ID_Contribucion; ?>">
            <div class="col-md-4">
                <label for="">fecha: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del patrocinador" class="form-control" name="Fecha_Contribucion" required value="<?php echo $editaCon->Fecha_Contribucion; ?>">
            </div>
            <div class="col-md-4">
                <label for=""> porcentaje: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar el orden del Planeta" class="form-control" required name="Porcentaje_Contribucion" value="<?php echo $editaCon->Porcentaje_Contribucion; ?>">
                <br>
            </div>
            <div class="col-md-4">
                <label for="ID_Articulo">Articulo: </label>
                <select name="ID_Articulo" class="form-control" id="ID_Articulo" required>
                    <?php foreach ($articulos as $articulo) : ?>
                        <option value="<?php echo $articulo->ID_Articulo; ?>"><?php echo $articulo->Titulo ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-4">
                <label for="ID_Autor">Autor: </label>
                <select name="ID_Autor" class="form-control" id="ID_Autor" required>
                    <?php foreach ($autores as $autor) : ?>
                        <option value="<?php echo $autor->ID_Autor; ?>"><?php echo $autor->Nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/contribuciones/listCo" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_editar_planeta").validate({
        rules: {
            nom_pla_recup_ba: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            orden_pla_recup_ba: {
                required: true,
                minlength: 1,
                maxlength: 3,
                digits: true,
            },
            foto_pla_recup_: {
                required: true,
                minlength: 5,
                maxlength: 250,
                letras: true,
            },
            dista_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 20,
                digits: true,
            },
            esta_pla_recup_ba: {
                required: true,
                minlength: 5,
                maxlength: 100,
                letras: true,
            }
        },
        messages: {
            nom_pla_recup_ba: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            orden_pla_recup_ba: {
                required: "Por favor ingrese el Orden del planeta",
                minlength: "El Orden debe tener al menos 1 digito",
                maxlength: "Orden Incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            foto_pla_recup_: {
                required: "Por favor ingresar la ciudad",
                minlength: "El apellido debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",
            },
            dista_pla_recup_ba: {
                required: "Por favor ingresa un número de distancia",
                minlength: "Distancia incorrecta ingrese al menos 5 digitos",
                maxlength: "Distancia incorrecta",
                digits: "Este campo solo acepta números",
                number: "Este campo solo acepta números",
            },
            esta_pla_recup_ba: {
                required: "Por favor ingresa un estado",
                minlength: "El estado debe tener al menos 5 caracteres",
                maxlength: "Estado muy extenso muy Extensa",

            }
        }
    });
</script>