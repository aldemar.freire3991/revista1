<h1 Align="center">Agregar Nueva Contribución</h1>

<form class="" id="frm_nuevo_contribucion" action="<?php echo site_url(); ?>/Contribuciones/guardaCo" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <label for="">Fecha: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar la fecha" class="form-control" name="Fecha_Contribucion" required value="">
            </div>
            <div class="col-md-4">
                <label for="">Porcentaje: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="number" placeholder="Ingresar el porcentaje" class="form-control" required name="Porcentaje_Contribucion" value="">
                <br>
            </div>
            <div class="col-md-6">
                <label for="ID_Articulo">Articulo: </label>
                <select name="ID_Articulo" class="form-control" id="ID_Articulo" required>
                    <?php foreach ($articulos as $articulo) : ?>
                        <option value="<?php echo $articulo->ID_Articulo; ?>"><?php echo $articulo->Titulo; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-6">
                <label for="ID_Autor">Autor: </label>
                <select name="ID_Autor" class="form-control" id="ID_Autor" required>
                    <?php foreach ($autores as $autor) : ?>
                        <option value="<?php echo $autor->ID_Autor; ?>"><?php echo $autor->Nombre; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/contribuciones/listCo" class="btn btn-danger">CANCELAR</a>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    $("#frm_nuevo_contribucion").validate({
        rules: {
            Fecha_Contribucion: {
                required: true,
            },
            Porcentaje_Contribucion: {
                required: true,
                number: true,
                min: 0,
                max: 100,
            },
            ID_Articulo: {
                required: true,
            },
            ID_Autor: {
                required: true,
            }
        },
        messages: {
            Fecha_Contribucion: {
                required: "Por favor ingresa la fecha",
            },
            Porcentaje_Contribucion: {
                required: "Por favor ingresa el porcentaje",
                number: "Porcentaje debe ser un número",
                min: "Porcentaje mínimo es 0",
                max: "Porcentaje máximo es 100",
            },
            ID_Articulo: {
                required: "Por favor selecciona un artículo",
            },
            ID_Autor: {
                required: "Por favor selecciona un autor",
            }
        }
    });
</script>
