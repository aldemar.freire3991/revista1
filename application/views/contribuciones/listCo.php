<h1 Align="center">Listar xontribuciones</h1>

<?php if($contribucion): ?>
    <table class="table table-striped text-center" id="tbl_planetas" >
        <thead class="text-center">
            <tr>
                <th>ID</th>
                <th>FECHA</th>
                <th>PORCENTAJE</th>
                <th> ARTICULO</th>
                <th>AUTOR</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($contribucion as $filaTemporal):?>
                <tr >
                    <td>
                        <?php echo $filaTemporal->ID_Contribucion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Fecha_Contribucion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->Porcentaje_Contribucion; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ID_Autor; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->ID_Articulo; ?>
                    </td>

                    <td >
                        <a href="<?php echo site_url(); ?>/contribuciones/editaCo/<?php echo $filaTemporal->ID_Contribucion; ?>" title="Editar co">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt=""></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/Contribuciones/eliminaCo/<?php echo $filaTemporal->ID_Contribucion; ?>" title="Eliminar co">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt=""></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE publicaciones ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_planetas").DataTable();
</script>
