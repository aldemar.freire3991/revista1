<?php
class Patrocinador extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("patrocinador", $datos);
    }

    function borrar($ID_Patrocinador)
        {
            $this->db->where("ID_Patrocinador", $ID_Patrocinador);
            return $this->db->delete("patrocinador");
        }

    function obtenerPa()
    {
        $listPa = $this->db->get("patrocinador");
        if ($listPa->num_rows() > 0) {
            return $listPa->result();
        } else {
            return false;
        }
    }

    function obtePla()
    {
        $listPla = $this->db->get("revista");
        if ($listPla->num_rows() > 0) {
            return $listPla->result();
        } else {
            return false;
        }
    }

    function editar($ID_Patrocinador, $dat)
        {
            $this->db->where("ID_Patrocinador", $ID_Patrocinador);
            return $this->db->update('patrocinador', $dat);
        }

        function obtenerPorId($ID_Patrocinador)
        {
            $this->db->where("ID_Patrocinador", $ID_Patrocinador);
            $patrocinador = $this->db->get("patrocinador");
            if ($patrocinador->num_rows() > 0) {
                return $patrocinador->row();
            }
            return false;
        }

}
