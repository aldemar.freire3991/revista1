<?php
    class Galaxia extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }

        function insertar($datos)
        {
            return $this->db->insert("comite", $datos);
        }

        function obtenerGala()
        {
            $listGala = $this->db->get("comite");
            if ($listGala->num_rows() > 0) {
                return $listGala->result();
            }else{
                return false;
            }
        }

        function borrar($ID_Comite)
        {
            $this->db->where("ID_Comite", $ID_Comite);
            return $this->db->delete("comite");
        }

        function editar($ID_Comite, $dat)
        {
            $this->db->where("ID_Comite", $ID_Comite);
            return $this->db->update('comite', $dat);
        }

        function obtenerPorId($ID_Comite)
        {
            $this->db->where("ID_Comite", $ID_Comite);
            $comite = $this->db->get("comite");
            if ($comite->num_rows() > 0) {
                return $comite->row();
            }
            return false;
        }


        
    }