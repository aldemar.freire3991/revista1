<?php
class Contribucion extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("contribucion", $datos);
    }

    function borrar($ID_Contribucion)
        {
            $this->db->where("ID_Contribucion", $ID_Contribucion);
            return $this->db->delete("contribucion");
        }

    function obtenerCo()
    {
        $listCo = $this->db->get("contribucion");
        if ($listCo->num_rows() > 0) {
            return $listCo->result();
        } else {
            return false;
        }
    }

    function obtenerAr()
    {
        $listAr = $this->db->get("articulo");
        if ($listAr->num_rows() > 0) {
            return $listAr->result();
        } else {
            return false;
        }
    }

    function obteAu()
    {
        $listAu = $this->db->get("autor");
        if ($listAu->num_rows() > 0) {
            return $listAu->result();
        } else {
            return false;
        }
    }

    function editar($ID_Contribucion, $dat)
        {
            $this->db->where("ID_Contribucion", $ID_Contribucion);
            return $this->db->update('contribucion', $dat);
        }

        function obtenerPorId($ID_Contribucion)
        {
            $this->db->where("ID_Contribucion", $ID_Contribucion);
            $contribucion = $this->db->get("contribucion");
            if ($contribucion->num_rows() > 0) {
                return $contribucion->row();
            }
            return false;
        }

}
