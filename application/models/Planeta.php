<?php
class Planeta extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("revista", $datos);
    }

    function borrar($ID_Revista)
        {
            $this->db->where("ID_Revista", $ID_Revista);
            return $this->db->delete("revista");
        }

    function obtenerPla()
    {
        $listPla = $this->db->get("revista");
        if ($listPla->num_rows() > 0) {
            return $listPla->result();
        } else {
            return false;
        }
    }

    function obteGala()
    {
        $listGala = $this->db->get("comite");
        if ($listGala->num_rows() > 0) {
            return $listGala->result();
        } else {
            return false;
        }
    }

    function editar($ID_Revista, $dat)
        {
            $this->db->where("ID_Revista", $ID_Revista);
            return $this->db->update('revista', $dat);
        }

        function obtenerPorId($ID_Revista)
        {
            $this->db->where("ID_Revista", $ID_Revista);
            $revista = $this->db->get("revista");
            if ($revista->num_rows() > 0) {
                return $revista->row();
            }
            return false;
        }

}
