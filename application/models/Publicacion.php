<?php
class Publicacion extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("publicacion", $datos);
    }

    function borrar($ID_Publicacion)
        {
            $this->db->where("ID_Publicacion", $ID_Publicacion);
            return $this->db->delete("publicacion");
        }

    function obtenerPu()
    {
        $listPu = $this->db->get("publicacion");
        if ($listPu->num_rows() > 0) {
            return $listPu->result();
        } else {
            return false;
        }
    }

    function obtePla()
    {
        $listPla = $this->db->get("revista");
        if ($listPla->num_rows() > 0) {
            return $listPla->result();
        } else {
            return false;
        }
    }

    function editar($ID_Publicacion, $dat)
        {
            $this->db->where("ID_Publicacion", $ID_Publicacion);
            return $this->db->update('publicacion', $dat);
        }

        function obtenerPorId($ID_Publicacion)
        {
            $this->db->where("ID_Publicacion", $ID_Publicacion);
            $publicacion = $this->db->get("publicacion");
            if ($publicacion->num_rows() > 0) {
                return $publicacion->row();
            }
            return false;
        }

}
