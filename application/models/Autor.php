<?php
    class Autor extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }

        function insertar($datos)
        {
            return $this->db->insert("autor", $datos);
        }

        function obtenerAu()
        {
            $listAu = $this->db->get("autor");
            if ($listAu->num_rows() > 0) {
                return $listAu->result();
            }else{
                return false;
            }
        }

        function borrar($ID_Autor)
        {
            $this->db->where("ID_Autor", $ID_Autor);
            return $this->db->delete("autor");
        }

        function editar($ID_Autor, $dat)
        {
            $this->db->where("ID_Autor", $ID_Autor);
            return $this->db->update('autor', $dat);
        }

        function obtenerPorId($ID_Autor)
        {
            $this->db->where("ID_Autor", $ID_Autor);
            $autor = $this->db->get("autor");
            if ($autor->num_rows() > 0) {
                return $autor->row();
            }
            return false;
        }


        
    }