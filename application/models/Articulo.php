<?php
class Articulo extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("articulo", $datos);
    }

    function borrar($ID_Articulo)
        {
            $this->db->where("ID_Articulo", $ID_Articulo);
            return $this->db->delete("articulo");
        }

    function obtenerAr()
    {
        $listAr = $this->db->get("articulo");
        if ($listAr->num_rows() > 0) {
            return $listAr->result();
        } else {
            return false;
        }
    }

    function obtePla()
    {
        $listPla = $this->db->get("revista");
        if ($listPla->num_rows() > 0) {
            return $listPla->result();
        } else {
            return false;
        }
    }

    function editar($ID_Articulo, $dat)
        {
            $this->db->where("ID_Articulo", $ID_Articulo);
            return $this->db->update('articulo', $dat);
        }

        function obtenerPorId($ID_Articulo)
        {
            $this->db->where("ID_Articulo", $ID_Articulo);
            $articulo = $this->db->get("articulo");
            if ($articulo->num_rows() > 0) {
                return $articulo->row();
            }
            return false;
        }

}
