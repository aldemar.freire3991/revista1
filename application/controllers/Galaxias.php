<?php
    class Galaxias extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('Galaxia');
        }

        public function nuegala()
        {
            $this->load->view('header');
            $this->load->view('galaxias/nuegala');
            $this->load->view('footer');
        }

        public function lisgala()
        {
            $data['comite'] = $this->Galaxia->obtenerGala();
            $this->load->view('header');
            $this->load->view('galaxias/lisgala', $data);
            $this->load->view('footer');
        }

        public function editaGala($ID_Comite)
        {
            $dat['editaGalax'] = $this->Galaxia->obtenerPorId($ID_Comite);
            $this->load->view('header');
            $this->load->view('galaxias/editaGala', $dat);
            $this->load->view('footer');
        }

        public function guardaGala()
        {
            $datosNueGala = array(
                "Nombre" => $this->input->post('Nombre'),
                "Direccion" => $this->input->post('Direccion'),
                "Descripcion" => $this->input->post('Descripcion')
            );
            if ($this->Galaxia->insertar($datosNueGala)) {
                redirect('galaxias/lisGala');
            } else {
                "<h1>Error al ingresar Datos :(</h1>";
            }
            
        }

        public function eliminaGala($ID_Comite)
        {
            if ($this->Galaxia->borrar($ID_Comite)) {
                redirect('galaxias/lisgala');
            }else{
                "<h1>Error al eliminar ._.</h1>";
            }
        }

        public function editarGalax()
        {
            $datosEditados = array(
                "Nombre" => $this->input->post('Nombre'),
                "Direccion" => $this->input->post('Direccion'),
                "Descripcion" => $this->input->post('Descripcion')
            );
            $ID_Comite = $this->input->post("ID_Comite");
            if ($this->Galaxia->editar($ID_Comite, $datosEditados)) {
                redirect('galaxias/lisgala');
            }
        }

    }