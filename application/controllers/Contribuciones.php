<?php
class Contribuciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Contribucion');
    }

    public function nueCo()
    {
        $datos['articulos'] = $this->Contribucion->obtenerAr();
        $datos['autores'] = $this->Contribucion->obteAu();
        $this->load->view('header');
        $this->load->view('contribuciones/nueCo', $datos);
        $this->load->view('footer');
    }

    public function listCo()
    {
        $data['contribucion'] = $this->Contribucion->obtenerCo();
        $this->load->view('header');
        $this->load->view('contribuciones/listCo', $data);
        $this->load->view('footer');
    }

    public function editaCo($ID_Contribucion)
    {
        $dat['articulos'] = $this->Contribucion->obtenerAr();
        $dat['autores'] = $this->Contribucion->obteAu();
        $dat['editaCon'] = $this->Contribucion->obtenerPorId($ID_Contribucion);
        $this->load->view('header');
        $this->load->view('contribuciones/editaCo', $dat);
        $this->load->view('footer');
    }



    public function guardaCo()
    {
        $datosNueCo = array(
            "Fecha_Contribucion" => $this->input->post('Fecha_Contribucion'),
            "Porcentaje_Contribucion" => $this->input->post('Porcentaje_Contribucion'),
            "ID_Articulo" => $this->input->post('ID_Articulo'),
            "ID_Autor" => $this->input->post('ID_Autor')

        );
       
        if ($this->Contribucion->insertar($datosNueCo)) {
            redirect('Contribuciones/listCo');
        } else {
            "<h1>Error al ingresar Datos :(</h1>";
        }
    }

    public function eliminaAr($ID_Contribucion)
    {
        if ($this->Contribucion->borrar($ID_Contribucion)) {
            redirect('contribuciones/listCo');
        } else {
            "<h1>Error al eliminar ._.</h1>";
        }
    }

    public function editarCo()
    {
        $datosEditados = array(
            "Fecha_Contribucion" => $this->input->post('Fecha_Contribucion'),
            "Porcentaje_Contribucion" => $this->input->post('Porcentaje_Contribucion'),
            "ID_Articulo" => $this->input->post('ID_Articulo'),
            "ID_Autor" => $this->input->post('ID_Autor')
        );
        $ID_Contribucion = $this->input->post("ID_Contribucion");
        if ($this->Contribucion->editar($ID_Contribucion, $datosEditados)) {
            redirect('contribuciones/listCo');
        }
    }
}
