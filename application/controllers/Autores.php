<?php
    class Autores extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('Autor');
        }

        public function nueau()
        {
            $this->load->view('header');
            $this->load->view('autores/nueau');
            $this->load->view('footer');
        }

        public function lisau()
        {
            $data['autor'] = $this->Autor->obtenerAu();
            $this->load->view('header');
            $this->load->view('autores/lisau', $data);
            $this->load->view('footer');
        }

        public function editaAu($ID_Autor)
        {
            $dat['editaAut'] = $this->Autor->obtenerPorId($ID_Autor);
            $this->load->view('header');
            $this->load->view('autores/editaAu', $dat);
            $this->load->view('footer');
        }

        public function guardaAu()
        {
            $datosNueAu = array(
                "Nombre" => $this->input->post('Nombre'),
                "Afiliacion" => $this->input->post('Afiliacion'),
                "Correo_Electronico" => $this->input->post('Correo_Electronico'),
                "Fecha_Nacimiento" => $this->input->post('Fecha_Nacimiento'),
                "Pais" => $this->input->post('Pais'),
                "Genero" => $this->input->post('Genero'),
                "Area_Especializacion" => $this->input->post('Area_Especializacion'),
                "ID_Revista_Principal" => $this->input->post('ID_Revista_Principal'),
                "Numero_Publicaciones" => $this->input->post('Numero_Publicaciones')
            );
            if ($this->Autor->insertar($datosNueAu)) {
                redirect('autores/lisAu');
            } else {
                "<h1>Error al ingresar Datos :(</h1>";
            }
            
        }

        public function eliminaAu($ID_Autor)
        {
            if ($this->Autor->borrar($ID_Autor)) {
                redirect('autores/lisau');
            }else{
                "<h1>Error al eliminar ._.</h1>";
            }
        }

        public function editarAut()
        {
            $datosEditados = array(
                "Nombre" => $this->input->post('Nombre'),
                "Afiliacion" => $this->input->post('Afiliacion'),
                "Correo_Electronico" => $this->input->post('Correo_Electronico'),
                "Fecha_Nacimiento" => $this->input->post('Fecha_Nacimiento'),
                "Pais" => $this->input->post('Pais'),
                "Genero" => $this->input->post('Genero'),
                "Area_Especializacion" => $this->input->post('Area_Especializacion'),
                "ID_Revista_Principal" => $this->input->post('ID_Revista_Principal'),
                "Numero_Publicaciones" => $this->input->post('Numero_Publicaciones')
            );
            $ID_Autor = $this->input->post("ID_Autor");
            if ($this->Autor->editar($ID_Autor, $datosEditados)) {
                redirect('autores/lisau');
            }
        }

    }