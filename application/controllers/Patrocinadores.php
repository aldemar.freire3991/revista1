<?php
class Patrocinadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Patrocinador');
    }

    public function nuePa()
    {
        $datos['patrocinador'] = $this->Patrocinador->obtePla();
        $this->load->view('header');
        $this->load->view('patrocinadores/nuePa', $datos);
        $this->load->view('footer');
    }

    public function listPa()
    {
        $data['patrocinador'] = $this->Patrocinador->obtenerPa();
        $this->load->view('header');
        $this->load->view('patrocinadores/listPa', $data);
        $this->load->view('footer');
    }

    public function editaPa($ID_Patrocinador)
    {
        $dat['patrocinador'] = $this->Patrocinador->obtePla();
        $dat['editaPat'] = $this->Patrocinador->obtenerPorId($ID_Patrocinador);
        $this->load->view('header');
        $this->load->view('patrocinadores/editaPa', $dat);
        $this->load->view('footer');
    }



    public function guardaPa()
    {
        $datosNuePa = array(
            "Nombre" => $this->input->post('Nombre'),
            "Sector" => $this->input->post('Sector'),
            "Tipo" => $this->input->post('Tipo'),
            "Contacto" => $this->input->post('Contacto'),
            "ID_Revista" => $this->input->post('ID_Revista')

        );
       
        if ($this->Patrocinador->insertar($datosNuePa)) {
            redirect('Patrocinadores/listPa');
        } else {
            "<h1>Error al ingresar Datos :(</h1>";
        }
    }

    public function eliminaPa($ID_Patrocinador)
    {
        if ($this->Patrocinador->borrar($ID_Patrocinador)) {
            redirect('patrocinadores/listPa');
        } else {
            "<h1>Error al eliminar ._.</h1>";
        }
    }

    public function editarPa()
    {
        $datosEditados = array(
            "Nombre" => $this->input->post('Nombre'),
            "Sector" => $this->input->post('Sector'),
            "Tipo" => $this->input->post('Tipo'),
            "Contacto" => $this->input->post('Contacto'),
            "ID_Revista" => $this->input->post('ID_Revista')
        );
        $ID_Patrocinador = $this->input->post("ID_Patrocinador");
        if ($this->Patrocinador->editar($ID_Patrocinador, $datosEditados)) {
            redirect('patrocinadores/listPa');
        }
    }
}
