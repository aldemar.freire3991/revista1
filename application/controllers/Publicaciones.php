<?php
class Publicaciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Publicacion');
    }

    public function nuePu()
    {
        $datos['publicacion'] = $this->Publicacion->obtePla();
        $this->load->view('header');
        $this->load->view('publicaciones/nuePu', $datos);
        $this->load->view('footer');
    }

    public function listPu()
    {
        $data['publicacion'] = $this->Publicacion->obtenerPu();
        $this->load->view('header');
        $this->load->view('publicaciones/listPu', $data);
        $this->load->view('footer');
    }

    public function editaPu($ID_Publicacion)
    {
        $dat['publicacion'] = $this->Publicacion->obtePla();
        $dat['editaPub'] = $this->Publicacion->obtenerPorId($ID_Publicacion);
        $this->load->view('header');
        $this->load->view('publicaciones/editaPu', $dat);
        $this->load->view('footer');
    }



    public function guardaPu()
    {
        $datosNuePu = array(
            "Fecha_Publicacion" => $this->input->post('Fecha_Publicacion'),
            "Volumen" => $this->input->post('Volumen'),
            "Numero" => $this->input->post('Numero'),
            "ID_Revista" => $this->input->post('ID_Revista')

        );
       
        if ($this->Publicacion->insertar($datosNuePu)) {
            redirect('Publicaciones/listPu');
        } else {
            "<h1>Error al ingresar Datos :(</h1>";
        }
    }

    public function eliminaPu($ID_Publicacion)
    {
        if ($this->Publicacion->borrar($ID_Publicacion)) {
            redirect('publicaciones/listPu');
        } else {
            "<h1>Error al eliminar ._.</h1>";
        }
    }

    public function editarPu()
    {
        $datosEditados = array(
            "Fecha_Publicacion" => $this->input->post('Fecha_Publicacion'),
            "Volumen" => $this->input->post('Volumen'),
            "Numero" => $this->input->post('Numero'),
            "ID_Revista" => $this->input->post('ID_Revista')
        );
        $ID_Publicacion = $this->input->post("ID_Publicacion");
        if ($this->Publicacion->editar($ID_Publicacion, $datosEditados)) {
            redirect('publicaciones/listPu');
        }
    }
}
