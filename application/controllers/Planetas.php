<?php
class Planetas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Planeta');
    }

    public function nuePla()
    {
        $datos['revista'] = $this->Planeta->obteGala();
        $this->load->view('header');
        $this->load->view('planetas/nuePla', $datos);
        $this->load->view('footer');
    }

    public function listPla()
    {
        $data['revista'] = $this->Planeta->obtenerPla();
        $this->load->view('header');
        $this->load->view('planetas/listPla', $data);
        $this->load->view('footer');
    }

    public function editaPla($ID_Revista)
    {
        $dat['revista'] = $this->Planeta->obteGala();
        $dat['editaPlan'] = $this->Planeta->obtenerPorId($ID_Revista);
        $this->load->view('header');
        $this->load->view('planetas/editaPla', $dat);
        $this->load->view('footer');
    }



    public function guardaPla()
    {
        $datosNuePla = array(
            "Nombre" => $this->input->post('Nombre'),
            "ISSN" => $this->input->post('ISSN'),
            "Sitio_Web" => $this->input->post('Sitio_Web'),
            "Fecha_Fundacion" => $this->input->post('Fecha_Fundacion'),
            "Tema" => $this->input->post('Tema'),
            "ID_Comite" => $this->input->post('ID_Comite')

        );
       
        if ($this->Planeta->insertar($datosNuePla)) {
            redirect('planetas/listPla');
        } else {
            "<h1>Error al ingresar Datos :(</h1>";
        }
    }

    public function eliminaPla($ID_Revista)
    {
        if ($this->Planeta->borrar($ID_Revista)) {
            redirect('planetas/listPla');
        } else {
            "<h1>Error al eliminar ._.</h1>";
        }
    }

    public function editarPla()
    {
        $datosEditados = array(
            "Nombre" => $this->input->post('Nombre'),
            "ISSN" => $this->input->post('ISSN'),
            "Sitio_Web" => $this->input->post('Sitio_Web'),
            "Fecha_Fundacion" => $this->input->post('Fecha_Fundacion'),
            "Tema" => $this->input->post('Tema'),
            "ID_Comite" => $this->input->post('ID_Comite')
        );
        $ID_Revista = $this->input->post("ID_Revista");
        if ($this->Planeta->editar($ID_Revista, $datosEditados)) {
            redirect('planetas/listPla');
        }
    }
}
