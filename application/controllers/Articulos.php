<?php
class Articulos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Articulo');
    }

    public function nueAr()
    {
        $datos['articulo'] = $this->Articulo->obtePla();
        $this->load->view('header');
        $this->load->view('articulos/nueAr', $datos);
        $this->load->view('footer');
    }

    public function listAr()
    {
        $data['articulo'] = $this->Articulo->obtenerAr();
        $this->load->view('header');
        $this->load->view('articulos/listAr', $data);
        $this->load->view('footer');
    }

    public function editaAr($ID_Articulo)
    {
        $dat['articulo'] = $this->Articulo->obtePla();
        $dat['editaArt'] = $this->Articulo->obtenerPorId($ID_Articulo);
        $this->load->view('header');
        $this->load->view('articulos/editaAr', $dat);
        $this->load->view('footer');
    }



    public function guardaAr()
    {
        $datosNueAr = array(
            "Titulo" => $this->input->post('Titulo'),
            "Resumen" => $this->input->post('Resumen'),
            "Palabras_Clave" => $this->input->post('Palabras_Clave'),
            "ID_Revista" => $this->input->post('ID_Revista')

        );
       
        if ($this->Articulo->insertar($datosNueAr)) {
            redirect('Articulos/listAr');
        } else {
            "<h1>Error al ingresar Datos :(</h1>";
        }
    }

    public function eliminaAr($ID_Articulo)
    {
        if ($this->Articulo->borrar($ID_Articulo)) {
            redirect('articulos/listAr');
        } else {
            "<h1>Error al eliminar ._.</h1>";
        }
    }

    public function editarAr()
    {
        $datosEditados = array(
            "Titulo" => $this->input->post('Titulo'),
            "Resumen" => $this->input->post('Resumen'),
            "Palabras_Clave" => $this->input->post('Palabras_Clave'),
            "ID_Revista" => $this->input->post('ID_Revista')
        );
        $ID_Articulo = $this->input->post("ID_Articulo");
        if ($this->Articulo->editar($ID_Articulo, $datosEditados)) {
            redirect('articulos/listAr');
        }
    }
}
